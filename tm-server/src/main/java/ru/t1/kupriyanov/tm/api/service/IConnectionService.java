package ru.t1.kupriyanov.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public interface IConnectionService {

    @NotNull
    SqlSession getSqlConnection();

    @NotNull
    SqlSessionFactory getSqlSessionFactory();

}
