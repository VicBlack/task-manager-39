package ru.t1.kupriyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.model.Session;

import java.util.Collection;
import java.util.List;

public interface ISessionService {

    @NotNull
    Session add(@Nullable Session session);

    @Nullable
    Session add(
            @Nullable String userId,
            @Nullable Session session
    );

    @NotNull
    Collection<Session> add(@NotNull Collection<Session> sessions);

    @NotNull
    Collection<Session> set(@NotNull Collection<Session> sessions);

    @NotNull
    List<Session> findAll();

    @NotNull
    List<Session> findAll(@Nullable String userId);

    @Nullable
    Session findOneById(@Nullable String id);

    @Nullable
    Session findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    Session findOneByIndex(@Nullable Integer index);

    @Nullable
    Session findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    Session removeOne(@Nullable Session session);

    Session removeOne(
            @Nullable String userId,
            @Nullable Session session
    );

    @Nullable
    Session removeOneById(@Nullable String id);

    @Nullable
    Session removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    Session removeOneByIndex(@Nullable Integer index);

    @Nullable
    Session removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    void removeAll();

    void removeAll(@Nullable String userId);

    int getSize();

    int getSize(@Nullable String userId);

    boolean existsById(String id);

}
