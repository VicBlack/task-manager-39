package ru.t1.kupriyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.enumerated.Status;
import ru.t1.kupriyanov.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @Nullable
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Project add(@Nullable Project project);

    @Nullable
    Project add(
            @Nullable String userId,
            @Nullable Project project
    );

    @NotNull
    Collection<Project> add(@NotNull Collection<Project> projects);

    @NotNull
    Collection<Project> set(@NotNull Collection<Project> projects);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@Nullable String userId);

    @Nullable
    Project findOneById(@Nullable String id);

    @Nullable
    Project findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    Project findOneByIndex(@Nullable Integer index);

    @Nullable
    Project findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    Project removeOne(@Nullable Project project);

    Project removeOne(
            @Nullable String userId,
            @Nullable Project project
    );

    @Nullable
    Project removeOneById(@Nullable String id);

    @Nullable
    Project removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    Project removeOneByIndex(@Nullable Integer index);

    @Nullable
    Project removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    void removeAll();

    void removeAll(@Nullable String userId);

    int getSize();

    int getSize(@Nullable String userId);

    boolean existsById(String id);

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    );


}
